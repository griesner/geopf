#!/usr/bin/python
import re

pattern = re.compile(r'^([0-9]+).*{(.*)}$')
pattern2 = re.compile(r'([0-9.]+)=([0-9]+)')
with open("all_fs_pois_distances") as f:
    for line in f:
        #print(line)
        for (idPOI, voisins) in re.findall(pattern, line):
            for (dist, voisin) in re.findall(pattern2, voisins):
                print(idPOI +'\t'+dist+'\t'+voisin)
