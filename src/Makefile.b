#CC = g++ -L/usr/local/lib -O3 -larmadillo -lgsl -lgslcblas -Wall
CC = g++ -O3 -larmadillo -Wall
OPTS = -L/usr/local/lib -I/usr/local/include -lgsl -lgslcblas -lm

LSOURCE = main.cpp utils.cpp data.cpp spf.cpp eval.cpp
CSOURCE = utils.cpp data.cpp eval.cpp

#all targets
all: spf profile pop rand mf librec_eval

# main model
spf: $(LSOURCE)
	  $(CC) $(LSOURCE) $(OPTS) -o spf

profile: $(LSOURCE)
	  $(CC) $(LSOURCE) $(OPTS) -o spf -pg


# comparison methods
pop: popularity.cpp $(CSOURCE)
	  $(CC) popularity.cpp $(CSOURCE) $(OPTS) -o pop

rand: random.cpp $(CSOURCE)
	  $(CC) random.cpp $(CSOURCE) $(OPTS) -o rand

mf: mf.cpp $(CSOURCE)
	  $(CC) mf.cpp $(CSOURCE) $(OPTS) -o mf

librec_eval: librec.cpp $(CSOURCE)
	  $(CC) librec.cpp $(CSOURCE) $(OPTS) -o librec_eval


# cleanup
clean:
	-rm -f spf pop mf librec_eval rand
