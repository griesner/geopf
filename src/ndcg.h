/*
 * ndcg.h
 *
 *  Created on: 31 d�c. 2015
 *      Author: Modou
 */

#ifndef METRICS_NDCG_H_
#define METRICS_NDCG_H_

#include "metric.h"

using namespace std;

namespace metrics {
class Ndcg : public Metric {
protected:
	// See http://code.google.com/p/cs221-awesome-team/source/browse/trunk/project3/Assignment3/src/ir/assignments/four/NDCG.java?r=157
	virtual double getRelevance(uint itemId, std::vector<uint>& test){
		size_t index = 0;
		for(; index < test.size(); index++)
			if(test[index] == itemId)
				break;

		if(index == test.size())
			return 0.0;
		return (double)(test.size() - index);
	}

	virtual double getIDCG(std::vector<uint>& test){
		double IDCG_k = 0.0;
		double relevance;
		for(size_t index=0; index < test.size(); index++){
			relevance = test.size() - index;
			IDCG_k += (pow(2.0, relevance) - 1.0) / (log2(index + 2));
		}
		return IDCG_k;
	}

	virtual double getDCG(std::vector<uint>& preds, std::vector<uint>& test, int nbItems_to_recommend){
		double DCG_k = 0.0;
		double relevance;
		for(int i=0; i < std::min((int)preds.size(), nbItems_to_recommend); i++){
			relevance = getRelevance(preds[i], test);
			if(relevance == 0)
				continue;
			DCG_k += (pow(2.0, relevance) - 1.0) / log2(2 + i);
		}
		return DCG_k;
	}

	virtual double score(std::vector<uint>& preds, std::vector<uint>& test, int nbItems_to_recommend = 30){
		if(test.empty() || preds.empty() || nbItems_to_recommend == 0)
			return 0.0;
		return getDCG(preds, test, nbItems_to_recommend) / getIDCG(test);
	}

public:
	Ndcg(std::string name, int nbRecommendations=5) : Metric(name, nbRecommendations){}

	virtual ~Ndcg(){}
};
}

#endif /* METRICS_NDCG_H_ */
