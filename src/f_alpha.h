/*
 * f_alpha.h
 *
 *  Created on: 31 d�c. 2015
 *      Author: Modou
 */

#include "recall.h"

#ifndef METRICS_F_ALPHA_H_
#define METRICS_F_ALPHA_H_

namespace metrics {
class F_alpha : public Recall {
protected:
	int alpha;

	virtual double score(std::vector<uint>& preds, std::vector<uint>& test, int nbItems_to_recommend = 30){
		if(test.empty() || preds.empty() || nbItems_to_recommend == 0)
			return 0.0;
		double precision = this->Precision::score(preds, test, nbItems_to_recommend);
		double recall = this->Recall::score(preds, test, nbItems_to_recommend);
		if(precision == 0.0 || recall == 0.0)
			return 0.0;
		double alpha_square = pow(alpha, 2);
		return ((1.0 + alpha_square) * precision * recall) / ((alpha_square * precision) + recall);
	}

public:
	F_alpha(std::string name, int nbRecommendations=5, int alpha = 1) : Recall(name, nbRecommendations){
		this->alpha = alpha;
	}

	virtual ~F_alpha(){}
};
}

#endif /* METRICS_F_ALPHA_H_ */
