
 // create model instance; learn!
    printf("\ncreating model instance\n");
    SPF *model = new SPF(&settings, dataset);
    printf("commencing model inference\n");
    model->learn();

    // double boucle sur les users et POIs : calculer tous les scores de la matrice userPOI pour calculer le score
    double prediction;
    vector<pair<int, double>> listePreds;
    map<int, vector<int>> userPOIsTest;
    // read in test data
    string fn = settings.datadir + "/test.tsv";
    FILE* fileptr = fopen(fn.c_str(), "r");
    int user, item, u, i;
    while ((fscanf(fileptr, "%d\t%d\t%d\n", &user, &item) != EOF)) {
        if(userPOIsTest.find(user) == userPOIsTest.end())
            userPOIsTest[user] = vector<int>();
        userPOIsTest[user].push_back(item);
    }
	
	// Initialize metrics
		vector<Metric*> metrics;
	metrics.push_back(new Precision("Precision"));
	metrics.push_back(new Recall("Recall"));
	metrics.push_back(new F_alpha("F1", 1));
	metrics.push_back(new Ndcg("NDCG"));
	metrics.push_back(new AvgPrecision("MAP"));

    for (map<int, vector<int>>::iterator it=userPOIsTest.begin(); it!=userPOIsTest.end(); it++) {  // for each user in test set
        listePreds.clear();
        for (int item = 0; item < model->getData()->item_count(); ++item) {
            prediction = model->predict(it->first, item);
            listePreds.push_back(make_pair(item, prediction));
        }
        std::sort(listePreds.begin(), listePreds.end(), sort_pred());
        // metric evaluation : measure(listePreds, it->second);
		for(vector<Metric*>::iterator it2=metrics.begin(); it2!=metrics.end(); it2++)
			(*it2)->measure(listePreds, it->second, nbRecommendations);
    }
    // inverser le test, regrouper les visits par user, préparer les valeurs de recommandation, regrouper set et écrire les métrics de qualité
    // prévoir un top K avec un K qui varie


    // test the final model fit
    printf("evaluating model on held-out data\n");
    model->evaluate();